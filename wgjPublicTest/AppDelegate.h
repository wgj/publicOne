//
//  AppDelegate.h
//  wgjPublicTest
//
//  Created by wangguanjun on 2017/4/14.
//  Copyright © 2017年 wangguanjun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

