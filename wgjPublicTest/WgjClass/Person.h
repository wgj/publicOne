//
//  Person.h
//  wgjPublicTest
//
//  Created by wangguanjun on 2017/4/14.
//  Copyright © 2017年 wangguanjun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
+ (void)personClick;
@end
